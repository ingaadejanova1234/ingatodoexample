import { ADDING_POST_FETCH_ERROR, ADD_POST, FETCH_POST } from './actionTypes'
import api from '../api'

export const fetchingCreatePostError = error => {
	return {
		type: ADDING_POST_FETCH_ERROR,
		error,
	}
}

export const createPostSuccess = data => {
	const { id, createdAt, title, priority, isDone } = data

	return {
		type: ADD_POST,
		payload: {
			id,
			createdAt,
			title,
			priority,
			isDone,
		},
	}
}

export const createPost = (title, description, priority, tags) => {
	const postData = {
		title,
		description,
		priority,
		tags,
	}
	return dispatch => {
		return api
			.postNewTodo(postData)
			.then(response => {
				dispatch(createPostSuccess(response.data))
			})
			.catch(error => {
				dispatch(fetchingCreatePostError(error.response.data.error.message))
			})
	}
}

export const fetchPostsSuccess = todolist => {
	return {
		type: FETCH_POST,
		todolist,
	}
}

export const fetchAllPosts = () => {
	return dispatch => {
		return api
			.getAllTodos()
			.then(response => {
				dispatch(fetchPostsSuccess(response.data))
			})
			.catch(error => {
				console.log(error) // dispatch action that handles error behavior
			})
	}
}

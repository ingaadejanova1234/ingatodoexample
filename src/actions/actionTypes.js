export const ADD_POST = 'ADD_POST'
export const FETCH_POST = 'FETCH_POST'
export const ADDING_POST_FETCH_ERROR = 'ADDING_POST_FETCH_ERROR'

export const SHOW_MODAL = 'SHOW_MODAL'
export const HIDE_MODAL = 'HIDE_MODAL'

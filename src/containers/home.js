import React from 'react'
import { connect } from 'react-redux'
import HomePage from '../components/templates/home-page'
import { createPost, fetchAllPosts, showModal } from '../actions/index'

const HomeContainer = ({ todolist, createPost, fetchAllPosts, showModal }) => {
	return (
		<HomePage
			showModal={showModal}
			todolist={todolist}
			createPost={createPost}
			fetchAllPosts={fetchAllPosts}
		/>
	)
}

const mapStateToProps = state => ({
	todolist: state.todolist,
})

const mapDispatchToProps = dispatch => ({
	createPost: (title, description, priority, tags) => {
		dispatch(createPost(title, description, priority, tags))
	},
	fetchAllPosts: () => {
		dispatch(fetchAllPosts())
	},
	showModal: (id, modalType) => dispatch(showModal(id, modalType)),
})

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(HomeContainer)

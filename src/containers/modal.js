import React from 'react'
import { connect } from 'react-redux'
import TodolistItemModal from '../components/blocks/todo-list-item-modal'
import { hideModal } from '../actions/index'

const MODAL_COMPONENTS = {
	TODO_LIST_ITEM_MODAL: TodolistItemModal,
	// other modals
}

const ModalRoot = ({ modalType, modalProps, hideModal }) => {
	if (!modalType) {
		return null
	}

	const SpecificModal = MODAL_COMPONENTS[modalType]
	return <SpecificModal {...modalProps} hideModal={hideModal} />
}

const mapStateToProps = state => ({
	...state.modal,
})

const mapDispatchToProps = dispatch => ({
	hideModal: () => {
		dispatch(hideModal())
	},
})

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(ModalRoot)

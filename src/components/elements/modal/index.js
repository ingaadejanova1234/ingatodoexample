import React from 'react'
import PropTypes from 'prop-types'

import Icon from '../../elements/icons'

import styles from './modal.scss'

export const Modal = ({ children, hideModal }) => {
	return (
		<div className={styles.root}>
			<div className={styles.children}>
				{children}
				<button onClick={() => hideModal()} className={styles.close}>
					<Icon type="close" />
				</button>
			</div>
		</div>
	)
}

Modal.propTypes = {
	children: PropTypes.array,
	hideModal: PropTypes.func,
}

export default Modal

import React, { useEffect } from 'react'
import CreatePostForm from '../../blocks/create-post-form'
import TodoListItem from '../../blocks/todo-list-item'
import styles from './home-page.scss'

const HomePage = ({
	createPost,
	todolist,
	fetchAllPosts,
	resetPosts,
	showModal,
}) => {
	useEffect(() => {
		fetchAllPosts()
	}, [])

	return (
		<div className={styles.container}>
			<CreatePostForm
				createPost={createPost}
				fetchAllPosts={fetchAllPosts}
				resetPosts={resetPosts}
				todoItemError={todolist.fetchErrorMessage}
			/>
			{(todolist.items || []).map(element => (
				<TodoListItem
					showModal={showModal}
					key={element.id}
					todoItem={element}
					fetchAllPosts={fetchAllPosts}
				/>
			))}
		</div>
	)
}

export default HomePage

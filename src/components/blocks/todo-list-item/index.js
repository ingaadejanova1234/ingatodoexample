import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styles from './todo-list-item.scss'
import api from '../../../api'

const TodoListItem = ({ todoItem, showModal }) => {
	const [isTodoItemDone, setIsTodoItemDone] = useState(todoItem.isDone)
	const todoItemDate = new Date(todoItem.createdAt)

	const completeTodo = e => {
		setIsTodoItemDone(e.target.checked)

		const postData = {
			isDone: !isTodoItemDone,
		}

		api.putTaskCompleted(todoItem.id, postData).catch(error => {
			console.log(error)
		})
	}

	const todoItemStatus = isTodoItemDone ? 'Task is Done!' : 'Task is not Done!'

	return (
		<div className={styles.root}>
			<input
				type="checkbox"
				checked={isTodoItemDone}
				onChange={e => completeTodo(e)}
			/>
			<div
				className={styles.info}
				onClick={() => showModal(todoItem.id, 'TODO_LIST_ITEM_MODAL')}
			>
				<h3>
					{todoItem.title} ({todoItem.priority}) - {todoItemStatus}
				</h3>
				<p>{todoItemDate.toString()}</p>
			</div>
		</div>
	)
}

TodoListItem.propTypes = {
	todoItem: PropTypes.object,
	showModal: PropTypes.func,
}

export default TodoListItem

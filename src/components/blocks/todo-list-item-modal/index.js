import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import Modal from '../../elements/modal'
import api from '../../../api'

const TodolistItemModal = ({ hideModal, id }) => {
	const [todoItemData, setIsTodoItemData] = useState([])

	useEffect(() => {
		api
			.getSingleTodo(id)
			.then(response => {
				setIsTodoItemData(response.data)
			})
			.catch(error => {
				console.log(error)
			})
	}, [])

	return (
		<Modal hideModal={hideModal}>
			<h1>{todoItemData.title}</h1>
			<p>{todoItemData.description}</p>
		</Modal>
	)
}

TodolistItemModal.propTypes = {
	id: PropTypes.string,
	hideModal: PropTypes.func,
}

export default TodolistItemModal

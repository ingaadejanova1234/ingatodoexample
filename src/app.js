import React from 'react'
import HomeContainer from './containers/home'
import ModalRoot from './containers/modal'

const App = () => {
	return (
		<>
			<HomeContainer />
			<ModalRoot />
		</>
	)
}

export default App

import {
	ADD_POST,
	FETCH_POST,
	ADDING_POST_FETCH_ERROR,
} from '../actions/actionTypes'

const initialState = {
	items: [],
	fetchErrorMessage: '',
}

export default (state = initialState, action) => {
	switch (action.type) {
		case ADD_POST:
			return {
				...state,
				items: state.items.concat(action.payload),
				fetchErrorMessage: '',
			}
		case FETCH_POST:
			return {
				...state,
				items: action.todolist,
				fetchErrorMessage: '',
			}
		case ADDING_POST_FETCH_ERROR:
			return {
				...state,
				fetchErrorMessage: action.error,
			}
		default:
			return state
	}
}

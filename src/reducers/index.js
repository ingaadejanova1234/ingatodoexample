import { combineReducers } from 'redux'
import todolist from './todolist'
import modal from './modal'

export default combineReducers({
	todolist,
	modal,
})

import axios from 'axios'
import { BASE_URI } from '../constants'

export default {
	postNewTodo: data => axios.post(`${BASE_URI}/todos`, { ...data }),
	getAllTodos: () => axios.get(`${BASE_URI}/todos`),
	resetTodos: () => axios.post(`${BASE_URI}/reset`),
	getSingleTodo: id => axios.get(`${BASE_URI}/todos/${id}`),
	putTaskCompleted: (id, data) =>
		axios.put(`${BASE_URI}/todos/${id}`, { ...data }),
}

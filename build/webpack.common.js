const path = require('path')
const autoprefixer = require('autoprefixer')

module.exports = {
	entry: {
		main: './src/index.js',
	},
	optimization: {
		splitChunks: {
			cacheGroups: {
				vendor: {
					test: /[\\/]node_modules[\\/](react|react-dom|axios)[\\/]/,
					name: 'vendors',
					chunks: 'all',
				},
			},
		},
	},
	module: {
		rules: [
			{
				test: /\.(js)$/,
				exclude: /(node_modules)/,
				loader: 'babel-loader',
			},
			{
				test: /\.scss$/,
				use: [
					'style-loader',
					{
						loader: `css-loader`,
						options: {
							importLoaders: 1,
							localIdentName: `[name]__[local]--[hash:base64:5]`,
							sourceMap: true,
						},
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: true,
							plugins: [autoprefixer()],
						},
					},
					{
						loader: 'sass-loader',
						options: {
							sourceMap: true,
							includePaths: [
								path.resolve('src/styles'),
								path.resolve('src/components'),
								path.resolve('node_modules'),
							],
						},
					},
				],
				include: path.resolve('src'),
			},
			{
				test: /\.html$/,
				use: [{ loader: 'html-loader' }],
			},
		],
	},
}
